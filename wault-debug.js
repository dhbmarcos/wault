// Wault 0.1.0
// D. H. B. Marcos (c) 2018. All rights reserved.

// Check that web crypto is even available
if (!window.crypto || !window.crypto.subtle) {
  document.body.innerHTML = "Your browser does not support the Web Cryptography API!";
}

// Check that encoding API is also available
if (!window.TextEncoder || !window.TextDecoder) {
  document.body.innerHTML = "Your browser does not support the Encoding API!";
}

// Convert array byffer to base 64 based code.
function arrayBufferToCode (arrayBuffer) {
  let buffer = [];
  for (let position of new Uint8Array (arrayBuffer)) {
    buffer.push (unescape (encodeURIComponent (String.fromCharCode (position))));
  };
  return btoa (buffer.join (''));
}

// Convert base 64 based code to array byffer.
function codeToArrayBuffer (code) {
  let buffer = [];
  code = decodeURIComponent (escape (atob (code)));
  for (let position = 0; position < code.length; position++) {
    buffer.push (code.charCodeAt (position));
  }
  return new Uint8Array (buffer).buffer;
}

// Derive a password string to use in encrypt.
async function derivekey (password) {
  encoder = new TextEncoder ("utf-8");
  key = await crypto.subtle.exportKey ('raw', await crypto.subtle.deriveKey ({
    name: 'PBKDF2',
    salt: encoder.encode ('salt 0'),
    iterations: 1000,
    hash: 'SHA-512'
  }, await crypto.subtle.importKey ('raw', encoder.encode (password), {name: 'PBKDF2'}, false, ['deriveKey']), {
    name: 'AES-GCM',
    length: 256
  }, true, ['encrypt', 'decrypt']));
  // return key;
  return arrayBufferToCode (key);
}

// Encrypt a text plan with a derived key password.
async function encrypt (key, text) {
  const initialVector = crypto.getRandomValues (new Uint8Array (12));
  return arrayBufferToCode (new TextEncoder ("utf-8").encode (arrayBufferToCode (initialVector) + ' ' + arrayBufferToCode (await crypto.subtle.encrypt ({
    name: 'AES-GCM',
    iv: initialVector
  }, await crypto.subtle.importKey ('raw', codeToArrayBuffer (key), {
    name: 'AES-GCM',
    iv: initialVector
  }, false, ['encrypt']), new TextEncoder ('utf-8').encode (text).buffer))));
}

// Decrypt a encrypted buffer with a derived key password.
async function decrypt (key, buffer) {
  const _buffer = (new TextDecoder ('utf-8').decode (codeToArrayBuffer (buffer))).split (' ');
  return new TextDecoder ('utf-8').decode (await crypto.subtle.decrypt ({
    name: 'AES-GCM',
    iv: codeToArrayBuffer (_buffer [0])
  }, await crypto.subtle.importKey ('raw', codeToArrayBuffer (key), {
    name: 'AES-GCM',
    iv: codeToArrayBuffer (_buffer [0])
  }, false, ['decrypt']), codeToArrayBuffer (_buffer [1])));
}
